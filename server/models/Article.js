const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    },
    //分类
    categories: [{
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Category'
    }]
},{
    timestamps: true
})

module.exports = mongoose.model('Article', schema)